import random
__author__ = "alex"

"""

"""


class Game(object):
    #@TODO: add variable for bonus points
    def __init__(self):
        self.players = []
        self.trump = None
        self.round = 1
        self.current_player = None
        self.first_player = None
        self.last_player = None
        self.cards = self.build_cards(self.round)
        self.total_bid = 0
        self.index = 0
        self.finished_round = False
        random.shuffle(self.cards)

    def deal_cards(self):
        for i in range(0, self.round):
            for p in self.players:
                p.add_card(self.cards.pop(0))

    #@TODO: only for testing purposes (I think). remove when done
    def get_players(self):
        return self.players

    def add_player(self, player):
        if len(self.players) >= 6:
            print("Already 6 players.")
            return

        self.players.append(player)

    def add_to_deck(self, card):
        self.cards.append(card)

    def remove_player(self, player):
        self.players.remove(player)

    def build_cards(self, count):
        c = []
        c.extend(self.make_suit(Suit.HEARTS, count))
        c.extend(self.make_suit(Suit.SPADES, count))
        c.extend(self.make_suit(Suit.DIAMOND, count))
        c.extend(self.make_suit(Suit.CLUBS, count))
        return c

    def make_suit(self, suit, count):
        s = []

        min = 15 - count * 2

        for i in range(min, 15):
            if i == 11:
                card = Card(suit, Card.ACE)
                s.append(card)
            else:
                card = Card(suit, i)
                s.append(card)
        return s

    def assign_cards(self):
        for p in self.players:
            self.assign_to_player(p)

    def assign_to_player(self, player):
        return

    def shuffle_cards(self):
        import random

        return random.shuffle(self.cards)

    def pick_trump(self):
        self.trump = self.cards.pop(0)

    def get_trump(self):
        return self.trump

    def get_round(self):
        return self.round

    def set_round(self, r):
        self.round = r

    def check_winner(self, round_cards):
        win = round_cards.pop()

        for c in round_cards:
            if not Card.same_suit(c, win):
                continue
            elif Card.compare(c, win) > 0:
                win = c

        win.get_player().add_count(1)

    def all_fail(self):
        for p in self.players:
            if p.get_bid() == p.get_count():
                return False

        return True

    def check_bonus(self):
        for p in self.players:
            if p.get_wins() == 5:
                p.add_points(10)
            else:
                p.get_points(-10)

    def get_current_player(self):
        return self.current_player

    def inc_current_player(self):
        self.index += 1
        if self.index == len(self.players) + 1:
            self.finished_round = True
            return
        self.current_player = self.players[self.index]

    def add_to_total_bid(self, val):
        self.total_bid += val

    def get_total_bid(self):
        return self.total_bid

    def reset_total_bid(self):
        self.total_bid = 0

    def start_game(self):
        self.shuffle_cards()
        self.deal_cards()
        self.pick_trump()
        self.first_player = self.players[self.index]
        self.last_player = self.players[-1]
        self.current_player = self.first_player
"""
"""


class Card(object):
    JACK = 12
    QUEEN = 13
    KING = 14
    ACE = 15
    #@TODO: Introduce a symbol variable with unicode value of suit
    def __init__(self, suit, value, player = None):
        self.suit = suit
        self.value = value
        self.player = player

    def get_suit(self):
        return self.suit

    def get_value(self):
        return self.value

    def __str__(self):
        s = ""
        if self.suit == Suit.HEARTS:
            s += u'\u2661'
        elif self.suit == Suit.DIAMOND:
            s += u'\u2662'
        elif self.suit == Suit.CLUBS:
            s += u'\u2663'
        elif self.suit == Suit.SPADES:
            s += u'\u2660'

        if self.value == 15:
            return "(" + "A" + s + ")"

        return "(" + str(self.value) + s + ")"

    def get_player(self):
        return self.player

    def set_player(self, player):
        self.player = player

    def remove_player(self):
        self.set_player(None)

    @staticmethod
    def same_suit(card_a, card_b):
        return card_a.get_suit() == card_b.get_suit()

    """
    This assumes same suit!!! Actually if different suitbut same value, 1 is returned (card_a wins)
    """
    @staticmethod
    def compare(card_a, card_b):
        if card_a.get_value() > card_b.get_value():
            return 1
        elif card_a.get_value == card_b.get_value():
            return 1
        else:
            return -1
"""
This is the suit.
"""


class Suit(object):
    HEARTS = 0
    SPADES = 1
    CLUBS = 2
    DIAMOND = 3

"""
"""


class Player(object):
    def __init__(self, name="Unknown"):
        self.name = name
        self.cards = []
        self.bid = 0
        self.count = 0
        self.wins = 0
        self.points = 0

    def add_card(self, card):
        self.cards.append(card)

    def remove_card(self, card):
        self.cards.remove(card)

    def clear_cards(self):
        self.cards.clear()

    def get_cards(self):
        return self.cards

    def print_cards(self):
        print("Cards for player " + self.name)
        for c in self.cards:
            print(c)
        print()

    def set_bid(self, bid):
        self.bid = bid

    def set_count(self, count):
        self.count = count

    def add_count(self, val):
        self.count += val

    def get_count(self):
        return self.count

    def get_bid(self):
        return self.bid

    def reset_wins(self):
        self.wins = 0

    def add_wins(self):
        if self.wins < 1:
            self.wins = 1
        else:
            self.wins += 1

    def subtract_wins(self):
        if self.wins > 1:
            self.wins = -1
        else:
            self.wins -= 1

    def get_wins(self):
        return self.wins

    def add_points(self, points):
        self.points += points

    def get_points(self):
        return self.points

    def get_name(self):
        return self.__str__()

    def __str__(self):
        return self.name

    def has_trump(self, trump):
        for c in self.cards:
            if Card.same_suit(c, trump):
                return True

        return False