from whist.Game import Game, Player

__author__ = 'alex'

import cmd

class CommandInterface(cmd.Cmd):
    def __init__(self):
        print("hi. starting cmd")
        self.completekey = None
        self.cmdqueue = None
        self.game = Game()
        self.can_bid = True
        self.can_pick = False

    def preloop(self):
        print("this is preloop")

    def postloop(self):
        print("this is postloop")

    def do_help(self, arg):
        print("Just do the on-screen shit")

    def do_exit(self, arg):
        print("BYE")
        return 0

    def do_shuffle(self):
        print("Shuffling")
        self.game.shuffle_cards()

    def do_pick(self, card):
        if not self.can_pick:
            print("Can't pick at this time")

        print("Picked " + str(card))
        self.game.inc_current_player()

        if self.game.get_current_player() == self.game.get_players()[-1]:
            print("Finished picking")

    def do_bid(self, arg):
        if not self.can_bid:
            print("Can't bid at this time")
            return
        elif int(arg) > self.game.get_round():
            print("Maximum bid is " + str(self.game.get_round()))
            return
        elif self.game.get_current_player() == self.game.get_players()[-1] and self.game.get_total_bid() + int(arg) == self.game.get_round():
            print("You are not allowed to bid " + arg)
            return

        player = self.game.get_current_player()
        player.set_bid(arg)
        self.game.add_to_total_bid(int(arg))
        print(player.get_name() + " bid " + arg)

        if self.game.get_current_player() == self.game.get_players()[-1]:
            self.can_bid = False
            self.can_pick = True
            print("Done bidding, now pick cards")
            self.game.inc_current_player()
            return

        self.game.inc_current_player()
        print("Please bid, " + self.game.get_current_player().__str__())
        self.game.get_current_player().print_cards()

    def do_addplayer(self, arg):
        p_list = str.split(arg, " ")
        if len(p_list) > 1:
            for p in p_list:
                self.do_addplayer(p)
        else:
            print("Adding player " + arg)
            player = Player(arg)
            self.game.add_player(player)

    def do_startserver(self, port):
        #check whether game is running
        print("starting server")

    def do_startgame(self, arg):
        print("Start")
        if len(self.game.get_players()) < 2:
            print("Can't start game: not enough players")
            return

        self.game.start_game()
        print("Trump: " + str(self.game.get_trump()))
        s = "Order: "

        for p in self.game.get_players():
            s += p.get_name() + " "

        print(s)

        print("Please bid, " + self.game.get_current_player().__str__())
        self.game.get_current_player().print_cards()

    def do_showcards(self, arg=None):
        if arg is None:
            for p in self.game.get_players():
                p.print_cards()
        else:
            for p in self.game.get_players():
                if p.get_name() == arg:
                    p.print_cards()
                    return

    def do_showtrump(self, arg):
        print(str(self.game.get_trump()))

    def do_connect(self, ip, port):
        print("Connecting")

    def do_disconnect(self):
        print("disconnecting")